const gulp = require('gulp');
const runSequence = require('run-sequence');
const $ = require('gulp-load-plugins')();
const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync');

const isProduction = process.env.NODE_ENV == 'production';

let config = {
  layoutName:   'purple',
  srcPath:      './src',
  destPath:     './dest',
  buildPath:    './build',
  layoutPath: {
    src:        './src/layout',
    dest:       './dest/layout'
  },
  fontAwesomePath: './node_modules/font-awesome'
};

const layoutBuildPath = `${config.buildPath}/layouts/${config.layoutName}`;
if (isProduction) {
  config.layoutPath.dest = layoutBuildPath;
}

// layout path를 모바일 레이아웃 path로 변경
function mLayoutPath(path) {
  return path.replace(/layout/, 'm.layout');
}

function taskInlineHtml(src, dest) {
  return gulp.src(src)
    .pipe($.inline({
      disabledTypes: ['js', 'css', 'img'], // Only inline svg files
    }))
    .pipe(gulp.dest(dest))
    .pipe(browserSync.stream());
}

function taskStyle(src, dest) {
  return gulp.src(src)
    .pipe($.replace('{font-awesome}', `../.${config.fontAwesomePath}`)) // font-awesome 경로 변경
    .pipe($.if(!isProduction, $.sourcemaps.init()))
    .pipe($.plumber())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.postcss([ autoprefixer() ]))
    .pipe($.if(!isProduction, $.sourcemaps.write(".")))
    .pipe($.if(isProduction, $.cssnano()))
    .pipe(gulp.dest(dest))
    .pipe(browserSync.stream({match: '**/*.css'}));
}

gulp.task('inline', () => {
  taskInlineHtml(
    `${config.layoutPath.src}/index.html`,
    `${config.layoutPath.dest}/`
  );
  taskInlineHtml(
    mLayoutPath(`${config.layoutPath.src}/index.html`),
    mLayoutPath(`${config.layoutPath.dest}/`)
  );
});

gulp.task('style', () => {
  taskStyle(
    `${config.srcPath}/scss/style.scss`,
    `${config.layoutPath.dest}/`
  );
  taskStyle(
    `${config.srcPath}/scss/mx.scss`,
    mLayoutPath(`${config.layoutPath.dest}/`)
  );
});

gulp.task('js', () => {
  gulp.src(`${config.srcPath}/js/mx.js`)
    .pipe($.if(!isProduction, $.sourcemaps.init()))
    .pipe($.babel())
    .pipe($.if(!isProduction, $.sourcemaps.write(".")))
    .pipe($.if(isProduction, $.uglify()))
    .pipe(gulp.dest(mLayoutPath(`${config.layoutPath.dest}/`)));
});

gulp.task('scss-lint', function() {
  return gulp.src(`${config.srcPath}/scss/**/*.scss`)
    .pipe($.scssLint());
});

// font-awesome copy
gulp.task('fa', () => {
  return gulp.src(`${config.fontAwesomePath}/fonts/*`)
    .pipe(gulp.dest(`${config.layoutPath.dest}/fonts/`))
    .pipe(gulp.dest(mLayoutPath(`${config.layoutPath.dest}/fonts/`)));
});

// build only
gulp.task('copy', () => {
  // meta tag icon
  gulp.src(`${config.srcPath}/icon/*`)
    .pipe(gulp.dest(`${config.layoutPath.dest}/icon/`))
    .pipe(gulp.dest(mLayoutPath(`${config.layoutPath.dest}/icon/`)));

  // svg (meta mask-icon에서 활용)
  gulp.src(`${config.srcPath}/svg/*`)
    .pipe($.svgmin())
    .pipe(gulp.dest(`${layoutBuildPath}/svg/`))
    .pipe(gulp.dest(mLayoutPath(`${layoutBuildPath}/svg/`)));

  // layout file
  gulp.src('layout/*.html')
    .pipe(gulp.dest(`${layoutBuildPath}/`));
  gulp.src('m.layout/*.html')
    .pipe(gulp.dest(mLayoutPath(`${layoutBuildPath}/`)));
});

// layout config
gulp.task('conf', () => {
  const now = new Date();
  const date = `${now.getFullYear()}. ${now.getMonth()+1}. ${now.getDate()}`;

  const package = require('./package.json');
  const replaceConfig = [{
    xpath: '//layout/@version',
    value: package.version
  }, {
    xpath: '//layout/title',
    value: package.title
  }, {
    xpath: '//layout/author/name',
    value: package.author.name
  }, {
    xpath: '//layout/author/@email_address',
    value: package.author.email
  }, {
    xpath: '//layout/author/@link',
    value: package.author.url
  }, {
    xpath: '//layout/author/@date',
    value: date
  }];

  gulp.src('layout/conf/info.xml')
    .pipe($.xmlpoke({ replacements: replaceConfig }))
    .pipe(gulp.dest(`${layoutBuildPath}/conf/`));

  // mobile
  gulp.src('m.layout/conf/info.xml')
    .pipe($.xmlpoke({ replacements: replaceConfig }))
    .pipe(gulp.dest(mLayoutPath(`${layoutBuildPath}/conf/`)));
});

// index.html
gulp.task('index', () => {
  return gulp.src('src/index.html')
    .pipe(gulp.dest(`${config.destPath}/`));
});

gulp.task('clean', () => {
  return gulp.src([
    config.destPath,
    config.buildPath,
  ]).pipe($.clean());
});

gulp.task('watch', () => {
  gulp.watch(`${config.srcPath}/js/**/*.js`, ['js']);
  gulp.watch(`${config.srcPath}/scss/**/*.scss`, ['scss-lint', 'style']);
  gulp.watch(`${config.srcPath}/**/index.html`, ['inline']).on('change', browserSync.reload);
});

gulp.task('serve', () => {
  browserSync.init({
    server: { baseDir: config.destPath },
    browser: []
  })
});

gulp.task('start', () => {
  runSequence(
    'clean',
    'index',
    'inline',
    'fa',
    'style',
    'js',
    'serve',
    'watch'
  );
});
