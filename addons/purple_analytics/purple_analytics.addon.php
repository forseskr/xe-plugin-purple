<?php
  if(!defined("__ZBXE__")) exit();

  /**
   * @file purple_analytics.addon.php
   * @author FORSES (master@forses.net)
   * @brief Google, NAVER Analytics 코드를 사이트에 추가
   **/

  // 관리자 모듈 제외
  if(Context::get('module')=='admin') return;

  // 한번만 출력시키기 위해 전역변수에 호출되었음을 체크해 놓음 (called position과 상관없음)
  if($GLOBALS['_called_addon_purple_analytics_']) return;
  $GLOBALS['_called_addon_purple_analytics_'] = true;

  $ga_code = '';
  $na_code = '';

  // Google Analytics
  if ($addon_info->ga_tracking_id) {
    $ga_code = <<<EndOfGA
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', '{$addon_info->ga_tracking_id}', 'auto');
ga('send', 'pageview');
</script>
EndOfGA;
  }

  // NAVER Analytics
  if ($addon_info->na_tracking_id) {
    $na_code = <<<EndOfGA
<script type="text/javascript" src="https://wcs.naver.net/wcslog.js"></script>
<script type="text/javascript">
if(!wcs_add) var wcs_add = {};
wcs_add["wa"] = "{$addon_info->na_tracking_id}";
wcs_do();
</script>
EndOfGA;
  }

  Context::addHtmlFooter($ga_code.$na_code);
?>
