(function() {
  const html = document.documentElement;
  const delegateEvent = (e) => {
    const navMenuButton = e.target && e.target.matches('.nav-toggle');
    if (navMenuButton) {
      html.classList.toggle('open-menu');
      window.scrollTo(0,0);
    }
  };
  html.addEventListener('click', delegateEvent);
})();
